import java.io.*;  
import java.net.*;
import java.util.HashMap; 
import java.util.Scanner; // Import the Scanner class to read text files
public class Master {  
	static DataInputStream din;
	static DataOutputStream dout;
	static DataInputStream din2;
	static DataOutputStream dout2;
	static String command,wtm1,wtm2;
	static String[] mtw, splitted;
	static HashMap<String, Integer> wordCount = new HashMap<String, Integer>();
	public static void main(String[] args){  
		try{  
			Socket s=new Socket("54.164.65.129",3001);
			System.out.println("connect to worker 1");
			Socket s2=new Socket("54.197.14.13",3002);
			System.out.println("connect to worker 2");
			din=new DataInputStream(s.getInputStream());
			dout=new DataOutputStream(s.getOutputStream());
			din2=new DataInputStream(s2.getInputStream());
			dout2=new DataOutputStream(s2.getOutputStream());
			BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
			command=""; wtm1="" ;wtm2="";
			
			while(!(command.equals("terminate@"))){  
				wordCount.clear();
				String stop="";
				command=br.readLine();
				mtw=command.split("@",2);
				if (mtw.length==2){
					if(mtw[0].equals("keywordCount")){
						splitted=splitString(mtw[1]);
						dout.writeUTF(mtw[0].concat("@").concat(splitted[0]));
						dout.flush();
						dout2.writeUTF(mtw[0].concat("@").concat(splitted[1]));
						dout2.flush();
					}else if(mtw[0].equals("keywordCountFile")){
						splitted=splitString(readFile(mtw[1]));
						dout.writeUTF("keywordCount@".concat(splitted[0]));
						dout.flush();
						dout2.writeUTF("keywordCount@".concat(splitted[1]));
						dout2.flush();
					}
					else if(mtw[0].equals("monitor")|mtw[0].equals("isDead")|mtw[0].equals("stop")|mtw[0].equals("terminate")|mtw[0].equals("sleep")){
						dout.writeUTF(command);
						dout.flush();
						dout2.writeUTF(command);
						dout2.flush();
					}
					else{
						System.out.println("Unimplemented Key Words, please use 'stop','monitor','keywordCount','keywordCountFile','isDead', or 'terminate'");
						continue;
					}
				}
				else{
					System.out.println("Command invalid, please separate keyword and argument with '@'");
					continue;
				}
				int a=din.available();
				int b=din2.available();
				if(a!=0&&b!=0){
					wtm1=din.readUTF();
					wtm2=din2.readUTF();
				}else{
					System.out.print("Stop? (1/2/both/no)");
					stop=br.readLine();
					a=din.available();
					b=din2.available();
					if (stop.equals("1")){
						dout.writeUTF("stop@");
						wtm2=din2.readUTF();
						a=din.available();
						if(a!=0) {wtm1=din.readUTF();}
						else {wtm1="{}";}
					}else if (stop.equals("2")){
						dout2.writeUTF("stop@");
						wtm1=din.readUTF();
						b=din2.available();
						if(b!=0) {wtm2=din2.readUTF();}
						else {wtm2="{}";}
					}else if (stop.equals("both")){
						dout.writeUTF("stop@");
						dout2.writeUTF("stop@");
						a=din.available();
						b=din2.available();
						if(a!=0) {wtm1=din.readUTF();}
						else {wtm1="{}";}
						if(b!=0) {wtm2=din2.readUTF();}
						else {wtm2="{}";}
					}else{
						wtm1=din.readUTF();
						wtm2=din2.readUTF();
					}
					
					
				}
				if(wtm1.charAt(0)=='{'){
					if(a!=0){
						wtm1=wtm1.substring(1,wtm1.length()-1);
						wordCount=countWord(wtm1.split(", "), wordCount);
					}
					if(b!=0){
						wtm2=wtm2.substring(1,wtm2.length()-1);					
						wordCount=countWord(wtm2.split(", "), wordCount);
					}
					System.out.println(wordCount);
					a=din.available();
					b=din2.available();
					
				}
				else{
					System.out.println("Worker Response:\n"+wtm1.toString()+"\n"+wtm2.toString());  
				}
			}
			din.close();  
			din2.close();
			s.close();  
			s2.close();  
		}
		catch(EOFException e){
			System.out.println("Worker terminated");
		}  
		catch(Exception e){
			System.out.println(e);
		}
	}
	private static String[] splitString(String str){
		String[] result={"0", "1"};
		
		if (str.split(" ",2).length<2){
			result[0]=str;
			result[1]=" ";
			return result;
		}
		int splitPoint = (int)str.length()/2;
		char currentChar = str.charAt(splitPoint);
		char SPACE = ' ';
		while (!(currentChar==SPACE)){
			splitPoint+=1;
			currentChar = str.charAt(splitPoint);
		}
		result[0]=str.substring(0,splitPoint);
		result[1]=str.substring(splitPoint+1);
		return result;
	}
	private static HashMap<String,Integer> countWord(String[] cutWords, HashMap<String,Integer> wordCount){
		String word;
		String count;
		for (int i=0;i<cutWords.length;i++){
			word=cutWords[i];
			count=word.split("=")[1];
			word=word.split("=")[0];
			if (wordCount.containsKey(word)){
				wordCount.put(word,wordCount.get(word)+Integer.parseInt(count));
			}
			else{
				wordCount.put(word,Integer.parseInt(count));
			}
		}
		return wordCount;
	}
	public static String readFile(String path) {
		try {
			File file = new File(path); 
			Scanner sc = new Scanner(file); 
			sc.useDelimiter("\\Z"); 
			return sc.next(); 
		}catch (FileNotFoundException e) {
		  System.out.println("An error occurred.");
		  e.printStackTrace();
		}
		return "File Not Found";
	}
	private static String[] splitString(String str){
		int splitPoint = (int)str.length()/2;
		char currentChar = str.charAt(splitPoint);
		char SPACE = ' ';
		String[] result={"0", "1"};
		while (!(currentChar==SPACE)){
			splitPoint+=1;
			currentChar = str.charAt(splitPoint);
		}
		result[0]=str.substring(0,splitPoint);
		result[1]=str.substring(splitPoint+1);
		return result;
	}
}  