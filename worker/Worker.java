import java.net.*;
import java.io.*;
//import org.json.simple.JSONObject;

/**
 * Worker class, use to listen for incoming TCP request.
 */
public class Worker {
    static boolean isRunning;
    static Thread runningThread;
    public static void main(String[] args) {
        try {
            ServerSocket ss = new ServerSocket(Integer.parseInt(args[0]));
            Socket s = ss.accept();
            DataInputStream din = new DataInputStream(s.getInputStream());
            isRunning = false;
            String str = "";
            while(!(str.equals("terminate"))) {
                str = din.readUTF();
                String[] functionArgs = str.split("@", 2);
                str = functionArgs[0];
                System.out.println(str);
                new Thread(new WorkerThread(s, functionArgs)).start();
            }
            din.close();
            ss.close();
        } catch(Exception e) {
            System.out.println("main err : " + e.toString());
        }
    }
}