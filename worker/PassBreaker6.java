import java.security.MessageDigest;
import java.nio.charset.StandardCharsets;

/**
 * Breaks a six character password
*/
class PassBreaker6 {
    static final String possibleChars = "abcdefghijklmnopqrstuvwxyz";
    static final int base = possibleChars.length();
    static final int wordLength = 5;
    static MessageDigest messageDigest;
    private static final byte[] HEX_ARRAY = "0123456789ABCDEF".getBytes(StandardCharsets.US_ASCII);

    public static String passBreak(String hash) {
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (Exception e) {
            System.out.println("md5 err : " + e.toString());
        }
        hash = hash.toUpperCase();
        String guess = "";
        String guessHash = "";
        double maxWords = Math.pow(base, wordLength);

        // Untuk setiap kata dari a, b, c,.., aa, .. zzzzz
        for(int i = 0; i < maxWords; i++) {
            guess = "";
            int currNum = i;

            // bagian generate kata
            do {
                int left = currNum % base;
                currNum = currNum / base;
                guess += String.valueOf(possibleChars.charAt(left)); // possibleChar = alfabet
            } while(currNum !=0);

            // cek apakah kata tersebut adalah passwordnya
            messageDigest.update(guess.getBytes());
            byte[] bytes = messageDigest.digest();
            guessHash = bytesToHex(bytes).toUpperCase();

            // jika benar maka kembalikan
            if(guessHash.equals(hash)) return guess;
        }
        return null;
    }

    // bytes to hex from: https://stackoverflow.com/questions/9655181/how-to-convert-a-byte-array-to-a-hex-string-in-java
    private static String bytesToHex(byte[] bytes) {
        byte[] hexChars = new byte[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars, StandardCharsets.UTF_8);
    }

    public static void main(String[] args) {
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (Exception e) {
            System.out.println("md5 err main : " + e.toString());
        }
        String pass1 = "tamfa";
        String pass2 = "luke";
        System.out.println("Check " + pass1);
        messageDigest.update(pass1.getBytes());
        byte[] bytes = messageDigest.digest();
        System.out.println(passBreak(bytesToHex(bytes).toUpperCase()));
        System.out.println("Check " + pass2);
        messageDigest.update(pass2.getBytes());
        bytes = messageDigest.digest();
        System.out.print(passBreak(bytesToHex(bytes).toUpperCase()));
    }
}