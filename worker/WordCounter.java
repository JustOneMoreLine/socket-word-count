import java.util.HashMap;

class WordCounter {
    private static String[] wordList;
    private static HashMap<String, Integer> wordCount = new HashMap<String, Integer>();

    public static String count(String text){
        // Replace special characters with space and change to lower case
        String textClean = text.replaceAll("[^a-zA-Z0-9]", " ").toLowerCase();
        // Split string by whitespace
        wordCount.clear();
        wordList = textClean.split("\\s+");
        for (String word: wordList){
            try {
                int count = wordCount.get(word);
                wordCount.put(word, ++count);
            } catch (NullPointerException e) {
                wordCount.put(word, 1);
            }
        }
        return wordCount.toString();
    }
}