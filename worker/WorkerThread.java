import java.io.DataOutputStream;
import java.net.Socket;

/**
 * Worker Thread, creates a thread for incoming request
 * A worker can only process one thread at a time, 
 * with the help of the Worker.running boolean,
 * Except monitoring functions.
 */
class WorkerThread implements Runnable {
    Socket socket;
    String[] args;
    public WorkerThread(Socket socket, String[] args) {
        this.args = args;
        this.socket = socket;
    }
    public void run() {
        DataOutputStream out = null;
        try {
            out = new DataOutputStream(socket.getOutputStream());
        } catch (Exception e) {
            System.out.println("output socket err :" + e.toString());
        }
        try {
            switch (this.args[0]) {
                case "stop":
                    if(Worker.isRunning) {
                        Worker.isRunning = false;
                        if(Worker.runningThread != null) Worker.runningThread.interrupt();
                        Worker.runningThread = null;
                        out.writeUTF("Job have been stopped");
                    } else {
                        out.writeUTF("No job have been stopped, since no jobs was runnning");
                    }
                    break;
                case "monitor":
                    if(Worker.isRunning) out.writeUTF("Running");
                    if(!(Worker.isRunning)) out.writeUTF("Avaiable");
                    break;
                case "keywordCount":
                    if(!(Worker.isRunning)) {
                        Worker.isRunning = true;
                        Worker.runningThread = Thread.currentThread();
                        out.writeUTF(WordCounter.count(this.args[1]));
                        Worker.runningThread = null;
                        Worker.isRunning = false;
                    } else {
                        out.writeUTF("Worker is busy.");
                    }
                    break;
                case "isDead":
                    out.writeUTF("Alive");
                    break;
                default:
                    System.out.print("switch miss");
                    Worker.isRunning = true;
                    Worker.runningThread = Thread.currentThread();
                    Thread.currentThread().sleep(10000);
                    Worker.runningThread = null;
                    Worker.isRunning = false;
                    break;
                }
        } catch (Exception e) {
            System.out.println("run err :" + e.toString());
        }

        /* temp function
        if(this.args[0].equals("monitor")) {
            try {
                if(Worker.running) {
                    out.writeUTF("Status: Running");
                } else {
                    out.writeUTF("Status: Available");
                }
            } catch (Exception e) {
                System.out.print("out.write err");
            }
        } else {
            try {
                if(!(Worker.running)) {
                    Worker.running = true;
                    System.out.println("Sleep");
                    Worker.runningThread = Thread.currentThread();
                    Thread.currentThread().sleep(10000);
                    System.out.println("Wake");
                    Worker.runningThread = 
                    Worker.running = false;
                }
            } catch (Exception e) {
                System.out.println("Sleep err");
            }
        }*/
    }
}
