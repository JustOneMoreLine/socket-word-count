import java.io.BufferedReader;
import java.net.*;
import java.io.*;

/**
 * Server dummy, for local testing
 */
public class ServerDummy {
    public static void main(String[] args) {
        try {
            BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
            Socket s = new Socket("localhost",3001);
            DataOutputStream dout = new DataOutputStream(s.getOutputStream()); 
            DataInputStream din = new DataInputStream(s.getInputStream()); 
            String str = "";
            while(!(str.equals("stop"))) {
                str = br.readLine();
                dout.writeUTF(str);
                if(str.equals("monitor")) {
                    String str2 = din.readUTF();
                    System.out.println(str2);
                }
            }
            s.close();
        } catch (Exception e) {
            System.out.println("dummy err");
        }
    }
}
